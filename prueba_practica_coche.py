import unittest
from programa_coche import Coche

class TestCoche(unittest.TestCase):

    
    def setUp(self): 
        self.car = Coche(colour='Azul', licensenumber='3228JRT', model='Countryman', brand='Mini Cooper', speed=85)
    # Esto crea una instancia con los valores y los parámetros que se marcan, para que puedan ser usados en los siguientes tests 
    
    def test_stopcurbing(self):
        self.car.curb(100)
        self.assertEqual(self.car.speed, 0)

    def test_decrease_speed_curb(self):
        self.car.curb(43)
        self.assertEqual(self.car.speed, 85 - 43)
        
if __name__ == '__main__':
    unittest.main()