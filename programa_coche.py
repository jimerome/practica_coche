class Coche:
    def __init__ (self, colour, licensenumber, model, brand, speed=0):
        self.colour = colour
        self.licensenumber = licensenumber
        self.model = model
        self.brand = brand
        self.speed = speed
        
        
    def accelerate (self, cantidad):
        self.speed += cantidad
    #def accelerate_reversing (self, cantidad_negativa): #esto sería para la deceleración
    #    self.speed += cantidad_negativa
        
        
    def curb (self, cantidad):
        self.speed = (self.speed - cantidad)
        if self.speed <= 0 : #si la cantidad resultante es negativa, el coche se para
            self.speed = 0
            print ('The car is not moving')
            
car = Coche(colour= 'Azul', licensenumber= '3228JRT', model= 'Countryman', brand= 'Mini Cooper', speed=85)

print('The car colour is:', car.colour)
print('The license number is:', car.licensenumber)
print('The brand is:', car.brand)
print('The car model is:', car.model)
print('The initial speed is:', car.speed, 'km/h')

#introduzco las pruebas para ver si el programa funciona

car.accelerate(43) #primero, para acelerar
print('The final speed after accelerating is', car.speed, 'km/h')

car.curb(43) #y para frenar
print('The final speed after curbing is', car.speed, 'km/h')


